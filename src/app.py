import logging

from flask import Flask

from src.patient.application.patient_resource import patient_bp
from src.patient.domain.patient_service import PatientService
from src.patient.infrastructure.patient_repository import PatientRepository


def create_app():
    app = Flask(__name__)

    logging.basicConfig(level=logging.INFO)

    # Configuration settings, routes, middleware, etc. can be added here
    patient_repository = PatientRepository()
    patient_service = PatientService(patient_repository)

    app.config['patient_service'] = patient_service

    app.register_blueprint(patient_bp)

    return app

if __name__ == "__main__":
    app = create_app()
    app.run(debug=True)

