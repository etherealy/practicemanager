import unittest
from pprint import pprint

from src.patient.infrastructure.patient_entity import PatientEntity
from src.patient.infrastructure.patient_repository import PatientRepository
from src.patient.domain.patient import Patient


class TestPatientRepository(unittest.TestCase):
    def setUp(self):
        self.repository = PatientRepository()

    def tearDown(self):
        pass

    def test_add_patient(self):
        patient_data = {
            'first_name': 'John',
            'last_name': 'Doe',
            'date_of_birth': '1980-01-01',
            'social_security_number': '123456789012345'
        }
        patient = Patient(**patient_data)

        new_patient = self.repository.add_patient(patient)
        pprint(vars(new_patient))

        self.assertEqual(self.repository.get_patient(new_patient.id), PatientEntity(**{'id': 1, **patient_data}))

    def test_get_patient(self):
        patient_data = {
            'first_name': 'John',
            'last_name': 'Doe',
            'date_of_birth': '1980-01-01',
            'social_security_number': '123456789012345'
        }
        patient = Patient(**patient_data)
        new_patient = self.repository.add_patient(patient)

        retrieved_patient = self.repository.get_patient(new_patient.id)

        self.assertEqual(retrieved_patient, patient)

    def test_get_patients(self):
        # Arrange
        patients_data = [
            {'first_name': 'John', 'last_name': 'Doe', 'date_of_birth': '1980-01-01',
             'social_security_number': '123456789012345'},
            {'first_name': 'Jane', 'last_name': 'Smith', 'date_of_birth': '1990-02-15',
             'social_security_number': '987654321012345'},
        ]
        patients = [Patient(**data) for data in patients_data]
        for patient in patients:
            self.repository.add_patient(patient)

        retrieved_patients = list(self.repository.get_patients())

        self.assertEqual(len(retrieved_patients), len(patients))
        for retrieved_patient, expected_patient in zip(retrieved_patients, patients):
            self.assertEqual(retrieved_patient, expected_patient)

    def test_update_patient(self):
        # Arrange
        patient_data = {
            'first_name': 'John',
            'last_name': 'Doe',
            'date_of_birth': '1980-01-01',
            'social_security_number': '123456789012345'
        }
        patient = Patient(**patient_data)
        new_patient = self.repository.add_patient(patient)

        updated_patient_data = {
            'first_name': 'UpdatedJohn',
            'last_name': 'UpdatedDoe',
            'date_of_birth': '1990-01-01',
            'social_security_number': '987654321098765'
        }
        updated_patient = Patient(**updated_patient_data)

        self.repository.update_patient(new_patient.id, updated_patient)

        retrieved_patient = self.repository.get_patient(patient.id)
        self.assertEqual(retrieved_patient, updated_patient)

    def test_delete_patient(self):
        # Arrange
        patient_data = {
            'first_name': 'John',
            'last_name': 'Doe',
            'date_of_birth': '1980-01-01',
            'social_security_number': '123456789012345'
        }
        patient = Patient(**patient_data)
        new_patient = self.repository.add_patient(patient)

        self.repository.delete_patient(new_patient.id)

        retrieved_patient = self.repository.get_patient(new_patient.id)
        self.assertIsNone(retrieved_patient)


if __name__ == '__main__':
    unittest.main()
