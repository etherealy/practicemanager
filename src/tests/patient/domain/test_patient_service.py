import unittest
import unittest.mock as mock
from src.patient.domain.patient_service import PatientService
from src.patient.domain.patient import Patient


class TestPatientService(unittest.TestCase):
    def setUp(self):
        self.mock_repository = mock.Mock()
        self.patient_service = PatientService(self.mock_repository)

    def test_create_patient(self):
        patient_data = {
            'first_name': 'John',
            'last_name': 'Doe',
            'date_of_birth': '1980-01-01',
            'social_security_number': 123456789012345
        }

        self.mock_repository.add_patient.return_value = {'id': 1, **patient_data}

        new_patient = self.patient_service.create_patient(Patient(**patient_data))

        self.assertIsNotNone(new_patient)
        self.assertEqual(new_patient['id'], 1)
        self.assertEqual(new_patient['first_name'], 'John')
        self.assertEqual(new_patient['last_name'], 'Doe')
        self.assertEqual(new_patient['date_of_birth'], '1980-01-01')
        self.assertEqual(new_patient['social_security_number'], 123456789012345)

        self.mock_repository.add_patient.assert_called_once()

    def assert_create_patient_with_missing_value_raises_value_error(self, missing_field):
        patient_data = {
            'first_name': 'John',
            'last_name': 'Doe',
            'date_of_birth': '1980-01-01',
            'social_security_number': '123456789012345'
        }
        del patient_data[missing_field]

        with self.assertRaises(ValueError):
            self.patient_service.create_patient(patient_data)

        self.mock_repository.create.assert_not_called()

    def test_create_patient_with_missing_first_name(self):
        self.assert_create_patient_with_missing_value_raises_value_error('first_name')

    def test_create_patient_with_missing_last_name(self):
        self.assert_create_patient_with_missing_value_raises_value_error('last_name')

    def test_create_patient_with_missing_date_of_birth(self):
        self.assert_create_patient_with_missing_value_raises_value_error('date_of_birth')

    def test_create_patient_with_missing_social_security_number(self):
        self.assert_create_patient_with_missing_value_raises_value_error('social_security_number')

    def test_create_patient_with_invalid_birth_date(self):
        patient_data = {
            'first_name': 'John',
            'last_name': 'Doe',
            'date_of_birth': '1980-01-32',
            'social_security_number': '123456789012345'
        }

        with self.assertRaises(ValueError):
            self.patient_service.create_patient(patient_data)

        self.mock_repository.create.assert_not_called()

    def assert_create_patient_raises_value_error(self, field, value):
        patient_data = {
            'first_name': 'John',
            'last_name': 'Doe',
            'date_of_birth': '1980-01-01',
            'social_security_number': '123456789012345'
        }
        patient_data[field] = value

        with self.assertRaises(ValueError):
            self.patient_service.create_patient(patient_data)

        self.mock_repository.create.assert_not_called()

    def test_create_patient_with_empty_first_name(self):
        self.assert_create_patient_raises_value_error('first_name', '')

    def test_create_patient_with_empty_last_name(self):
        self.assert_create_patient_raises_value_error('last_name', '')

    def test_create_patient_with_empty_date_of_birth(self):
        self.assert_create_patient_raises_value_error('date_of_birth', '')

    def test_create_patient_with_empty_social_security_number(self):
        self.assert_create_patient_raises_value_error('social_security_number', '')

    def test_get_all_patients(self):
        expected_patients = [
            {'id': 1, 'first_name': 'John', 'last_name': 'Doe', 'dob': '1980-01-01'},
            {'id': 2, 'first_name': 'Jane', 'last_name': 'Smith', 'dob': '1990-02-15'},
        ]

        self.mock_repository.get_all.return_value = expected_patients

        actual_patients = self.patient_service.get_all_patients()

        self.assertEqual(actual_patients, expected_patients)

    # TODO: add tests to paginate get_all_patients

    def test_get_patient(self):
        patient_id = 1
        expected_patient_info = {
            'id': 1,
            'first_name': 'John',
            'last_name': 'Doe',
            'date_of_birth': '1980-01-01',
            'social_security_number': '123456789012345'
        }

        self.mock_repository.get.return_value = expected_patient_info

        actual_patient_info = self.patient_service.get_patient(patient_id)

        self.assertEqual(actual_patient_info, expected_patient_info)
        self.mock_repository.get.assert_called_once_with(patient_id)

    def test_update_patient(self):
        patient_id = 1
        updated_patient_data = {
            'id': patient_id,
            'first_name': 'UpdatedFirstName',
            'last_name': 'UpdatedLastName',
            'dob': '1995-05-20',
            'social_security_number': '987654321012345'
        }

        self.mock_repository.update.return_value = updated_patient_data

        updated_patient = self.patient_service.update_patient(patient_id, updated_patient_data)

        self.assertEqual(updated_patient, updated_patient_data)
        self.mock_repository.update.assert_called_once_with(patient_id, updated_patient_data)

    # TODO: add tests to allow partial updates to patient (PATCH)

    def test_delete_patient(self):
        patient_id = 1

        self.patient_service.delete_patient(patient_id)

        self.mock_repository.delete.assert_called_once_with(patient_id)


if __name__ == '__main__':
    unittest.main()
