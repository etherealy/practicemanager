import unittest
from unittest.mock import Mock

from flask.testing import FlaskClient

from src.patient.domain.patient import Patient
from src.patient.infrastructure.patient_entity import PatientEntity
from ....app import create_app
from ....patient.domain.patient_service import PatientService


class TestPatientAPI(unittest.TestCase):
    def setUp(self):
        self.app = create_app()
        self.app.config['TESTING'] = True
        self.client: FlaskClient = self.app.test_client()

        self.mock_service = Mock(spec=PatientService)
        self.app.config['patient_service'] = self.mock_service

    def tearDown(self):
        pass

    def test_get_patient_endpoint(self):
        patient_id = 1
        expected_patient = {
            'id': patient_id,
            'first_name': 'John',
            'last_name': 'Doe',
            'date_of_birth': '1980-01-01',
            'social_security_number': '123456789012345'
        }

        self.mock_service.get_patient.return_value = expected_patient

        response = self.client.get(f'/patients/{patient_id}')

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json, expected_patient)
        self.mock_service.get_patient.assert_called_once_with(patient_id)

    def test_create_patient_endpoint(self):
        patient_data = {
            'first_name': 'John',
            'last_name': 'Doe',
            'date_of_birth': '1980-01-01',
            'social_security_number': 123456789012345,
        }

        self.mock_service.create_patient.return_value = PatientEntity(1, **patient_data)

        response = self.client.post('/patients', json=patient_data)


        print(response.json)


        self.assertEqual(201, response.status_code)
        self.assertEqual(response.json, PatientEntity(1, **patient_data).to_dict())
        self.mock_service.create_patient.assert_called_once()

if __name__ == '__main__':
    unittest.main()