import logging

from src.patient.domain.patient import Patient
from src.patient.infrastructure.patient_entity import PatientEntity


class PatientRepository:
    def __init__(self):
        self.db = {}
        self.next_patient_id = 1

    def get_patient(self, patient_id):
        return self.db.get(patient_id)

    def get_patients(self):
        return self.db.values()

    # def add_patient(self, patient):
    #     logging.info(f'Patient added: {patient}')
    #     patient_id = len(self.db) + 1
    #     patient_entity = PatientEntity(patient_id, patient.first_name, patient.last_name, patient.date_of_birth,
    #                                       patient.social_security_number)
    #     # patientEntity.id = len(self.db) + 1
    #     logging.info(f'Patient added2: {patient}')
    #     self.db.update({1: patient})
    #     return patient

    def add_patient(self, patient: Patient):
        # Generate a uniq patient ID
        patient_id = self.next_patient_id
        self.next_patient_id += 1
        logging.info(f'Patient ID generated: {patient_id}')

        # Create a PatientEntity with ID
        patient_entity = PatientEntity(
            id=patient_id,
            first_name=patient.first_name,
            last_name=patient.last_name,
            date_of_birth=patient.date_of_birth,
            social_security_number=patient.social_security_number
        )
        logging.info(f'Patient added: {patient_entity}')
        # Store the patient entity in the database
        self.db[patient_id] = patient_entity

        # Return the created patient entity
        return patient_entity

    def update_patient(self, patient_id, patient):
        return self.db.update({patient_id: patient})

    def delete_patient(self, patient_id):
        return self.db.pop(patient_id)
