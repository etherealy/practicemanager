import logging

from flask import Blueprint, jsonify, request
from flask import current_app as app

from src.patient.domain.patient import Patient

patient_bp = Blueprint('patient', __name__)


@patient_bp.route('/patients/<int:patient_id>', methods=['GET'])
def get_patient(patient_id):
    patient_service = app.config['patient_service']

    try:
        patient = patient_service.get_patient(patient_id)
        return jsonify(patient.to_dict()), 200
    except Exception as e:
        return jsonify({'error': str(e)}), 500


@patient_bp.route('/patients', methods=['POST'])
def create_patient():
    try:
        patient_data = request.json
        patient_service = app.config['patient_service']
        created_patient = patient_service.create_patient(Patient(**patient_data))
        logging.info(jsonify(created_patient.to_dict()))

        return jsonify(created_patient.to_dict()), 201

    except Exception as e:
        return jsonify({'error': str(e)}), 400
